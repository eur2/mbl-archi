export const prerender = true

export const load = async () => {
  const fetchPost = async () => {
      const res = await fetch(`https://api.mbl.archi/wp-json/wp/v2/pages?slug=info`)
      const data = await res.json()
      return data[0]
  }
  const fetchPost2 = async () => {
    const res = await fetch(`https://api.mbl.archi/wp-json/wp/v2/pages?slug=info-fr`)
    const data = await res.json()
    return data[0]
}

  return {
      info: fetchPost(),
      infofr: fetchPost2(),
  }
}
